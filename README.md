# K8S Namespace Ansible Role

Ansible role to create or remove a kubernetes namespace.

## Usage

```
- hosts: all
  gather_facts: false
  tasks:
    - include_role: 
        name: ansible-role-k8s-namespace
      vars:
        k8s_namespace_action: create # use "remove" to delete namespace
```

## License

[Apache-2.0](./LICENSE)
